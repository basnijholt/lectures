# Semiconductor physics

## Review of band structure properties

* Group velocity $v=\hbar^{-1}\partial E(k)/\partial k$
* Effective mass $m_{eff} = \hbar^2\left(d^2 E(k)/dk^2\right)^{-1}$
* Density of states $g(E) = \sum_{\textrm{FS}}dk \times (dn/dk) \times (dk/dE)$ the amount of states per infinitesimal interval of energy at each energy.

A simple check that everything is correct is the free electron model:

$$H = \hbar^2 k^2/2m$$

The velocity is $\hbar^{-1}\partial E(k)/\partial k = \hbar k / m \equiv p/m$.  
The effective mass is $m_{eff} = \hbar^2\left(d^2 E(k)/dk^2\right)^{-1} = m$.

So in the simplest case the definitions match the usual expressions.

#### Why are these properties important?

* Group velocity descibes how quickly electrons interacting with the lattice move.
* Effective mass tells us how hard it is to *accelerate* the particles and it enters $g(E)$ for a parabolic band
* Density of states is required to determine the Fermi level, heat capacity, etc.

## Filled vs empty bands

A completely filled band is very similar to a completely empty band.

In a filled band $n(E)=1$ because $|E - E_F| \gg kT$. In an empty band $n(E)=0$.

Heat capacity $C_v = \tfrac{d}{dT}\int_{-\infty}^\infty E\times g(E) \times dE\times n_F(E, T) = 0$.

A completely filled band carries no electric current:

$$
j = 2e \int_{-\pi/a}^{\pi/a} v(k) dk = 2e \int_{-\pi/a}^{\pi/a} (dE/dk)\times dk = 2e [E(-\pi/a) - E(\pi/a)] = 0
$$

## From electrons to holes

A completely filled band $\approx$ completely empty band. The idea of introducing **holes** is to transform an *almost* completely filled band $\Rightarrow$ almost empty one. Instead of describing a lot of electrons that are present, we focus on those that are absent.

Definition: a **hole** is a state of a completely filled band with one particle missing.

![](figures/holes.svg)

In this schematic we can either say that 8×2 electron states are occupied (the system has 8×2 electrons counting spin), or 10×2 hole states are occupied. A useful analogy to remember: glass half-full or glass half-empty.

## Properties of holes

The probability for an electron state to be occupied in equilibrium is $f(E)$:

$$f(E) = \frac{1}{e^{(E-E_F)/kT} + 1}$$

The probability for a hole state to be occupied:

$$1 - f(E) = 1 - \frac{1}{e^{(E-E_F)/kT} + 1} = \frac{1}{e^{(-E+E_F)/kT} + 1}$$

therefore for holes both energy $E_h= -E$ and $E_{F,h} = -E_F$.

The **momentum** $p_h$ of a hole should give the correct total momentum of a partially filled band if one sums momenta of all holes. Therefore $p_h = -\hbar k$, where $k$ is the wave vector of the electron.

Similarly, the total **charge** should be the same regardless of whether we count electrons or holes, so holes have a positive charge $+e$ (electrons have $-e$).

On the other hand, the velocity of a hole is **the same**:
$$\frac{dE_h}{dp_h} = \frac{-dE}{-d\hbar k} = \frac{dE}{dp}$$

Finally, we derive the hole effective mass from the equations of motion:

$$m_h \frac{d v}{d t} = +e (E + v\times B)$$

Comparing with

$$m_e \frac{d v}{d t} = -e (E + v\times B)$$

we get $m_h = -m_e$.

## Semiconductors: materials with two bands.

In semiconductors the Fermi level is between two bands. The unoccupied band is the **conduction band**, the occupied one is the **valence band**. In the conduction band the **charge carriers** (particles carrying electric current) are electrons, in the valence band they are holes.

We can control the position of the Fermi level (or create additional excitations) making semiconductors conduct when needed.

Only the bottom of the conduction band has electrons and the top of the valence band has holes because the temperature is always smaller than the width of the band.

Therefore we can approximate the dispersion relation of both bands as parabolic.

![](figures/semiconductor.svg)

Or in other words

$$E_e = E_G + \frac{\hbar^2k^2}{2m_e},$$
$$E_h = \frac{\hbar^2k^2}{2m_h}.$$

Here $E_G$ is the band gap, and the top of the valence band is at $E=0$.

Observe that because we are describing particles in the valence band as holes, $m_h > 0$ and $E_h > 0$.

**Question:** a photon gives a single electron enough energy to move from the valence band to the conduction band. How many particles does this process create?

## Semiconductor density of states and Fermi level

### Part 1: pristine semiconductor

![](figures/intrinsic_DOS.svg)

Electrons
$$ E = E_G + {p^2}/{2m_e}$$

$$ g(E) = (2m_e)^{3/2}\sqrt{E-E_G}/2\pi^2\hbar^3$$

Holes
$$ E = {p^2}/{2m_h}$$

$$ g(E) = (2m_h)^{3/2}\sqrt{-E}/2\pi^2\hbar^3$$

**The key algorithm of describing the state of a semiconductor:**

1. Write down the density of states, assuming a certain position of the Fermi level
2. Calculate the total amount of electrons and holes, equate the difference to the total amount of electrons $-$ holes available.
3. Use physics intuition to simplify the equations (this is important!)
4. Find $E_F$ and concentrations of electrons and holes

Applying the algorithm:

$$n_h = \int_0^\infty f_h(E+E_F)g_h(E)dE = \int_0^\infty\frac{(2m_h)^{3/2}}{2\pi^2\hbar^3} \sqrt{E}\frac{1}{e^{(E+E_F)/kT}+1}dE$$

$$n_e = \int_{E_G}^\infty f_h(E-E_F)g_e(E)dE = \int_{E_G}^\infty\frac{(2m_e)^{3/2}}{2\pi^2\hbar^3} \sqrt{E-E_G}\frac{1}{e^{(E-E_F)/kT}+1}dE$$

We need to solve $n_e = n_h$

Simplification:
Fermi level is far from both bands $E_F \gg kT$ and $E_G - E_F \gg kT$

Therefore Fermi-Dirac distribution is approximately similar to Boltzmann distribution.

$$f(E\pm E_F) \approx e^{-(E\pm E_F)/kT}$$

Now we can calculate $n_e$ and $n_h$:

$$n_h \approx \frac{V(2m_h)^{3/2}}{2\pi^2\hbar^3}e^{-E_F/kT} \int_0^\infty\sqrt{E}e^{-E/kT}dE =
N_V e^{-E_F/kT},$$

with

$$N_V = 2\left(\frac{2\pi m_h kT}{h^2}\right)^{3/2}$$

the number of holes with energy $E<T$ (compare with the rule above).

**Question:** how large is $N_V$?

**Answer:** If $kT \sim \textrm{eV}$, then $N_V \sim 1$. Therefore $N_V \sim (kT/\textrm{eV})^{3/2}\sim 1\%$.

Similarly for electrons:

$$n_e = N_C e^{-(E_G - E_F)/kT},\quad N_C = 2\left(\frac{2\pi m_e kT}{h^2}\right)^{3/2}$$


Combining everything together:

$$n_h \approx N_V e^{-E_F/kT} = N_C e^{-(E_G-E_F)/kT} \approx n_e$$

Solving for $E_F$:

$$E_F = \frac{E_G}{2} - \frac{3}{4}kT\log(m_e/m_h)$$

An extra observation: regardless of where $E_F$ is located, $n_e n_h = N_C N_V e^{-E_G/kT} \equiv n_i^2$.

$n_i$ is the **intrinsic carrier concentration**, and for a pristine semiconductor $n_e = n_h = n_i$.

> The equation
> $$n_e n_h = n_i^2$$
> is the **law of mass action**. The name is borrowed from chemistry, and describes the equilibrium concentration of two reagents in a reaction $A+B \leftrightarrow AB$. Here electrons and hole constantly split and recombine.

## Adding an impurity to semiconductor

* Typical semiconductors are group IV (Si, Ge, GaAs).
* Unfilled shell of group V atom (donor) has 1 extra electron and its nucleus 1 extra proton
* Group III atom (acceptor) lacks 1 electron and 1 nucleus charge

Extra electron (or extra hole) is attracted to the extra charge of the nucleus.

In H the energy levels are:
$$ E_n = - \frac{me^4}{8\pi^2\hbar^3\varepsilon^2_0n^2} = -R_E /n^2= -\frac{13.6\text{eV}}{n^2}$$

Bohr radius (size of the ground state wave function): $4 \pi \varepsilon_0 \hbar^2/m_{\mathrm{e}} e^2$

In a semiconductor $m\to m_{\text{eff}}$, $\epsilon_0 \to \epsilon\epsilon_0$.

An impurity creates a very weakly bound state:
$$E = -\frac{m_e}{m\varepsilon^2} R_E = -0.01 \text{eV (in Ge)}$$
$r = 4$ nm (vs $r = 0.5$ Å in H).

Binding energy smaller than room temperature (0.026 eV).

So a donor adds an extra state at $E_D$ (close to the bottom of the conduction band) and an extra electron.

Likewise an acceptor adds an extra state at $E_A$ (close to the top of the valence band) and an extra hole.

### Density of states with donors and acceptors

![](figures/doping.svg)

All donor/acceptor states at the same energy:
$$g_A(E) = N_A \delta(E-E_A),\quad g_D(E) = N_D \delta(E- (E_G - E_D))$$

How large can $N_D/N_A$ be? The distance between donors should be such that the states don't overlap, so the distance must be much larger than 4 nm. Therefore **maximal** concentration of donors before donor band merges with conduction band is $N_D \lesssim (1Å/4\textrm{nm})^3 \sim 10^{-5}\ll N_C$.


## Number of carriers

Charge conservation:
$$n_e - n_h + n_D - n_A = N_D - N_A$$

We already know $n_e$ and $n_h$.

$$n_D = N_D \frac{1}{e^{-(E_D-E_F)/kT} + 1}, n_A = N_A \frac{1}{e^{-(E_A+E_F)/kT} + 1}$$

Simplification:

Most donors are ionized and most acceptors are occupied.

Then

$$n_e - n_h = N_D - N_A, n_e = n_i^2/n_h$$

When $|N_D-N_A| \gg n_i$ the semiconductor is **extrinsic**, so that if $N_D > N_A$ ($n$-doped semiconductor), $n_e \approx N_D -N_A$ and $n_h = n_i^2/(N_D-N_A)$. If $N_D < N_A$ ($p$-doped semiconductor), $n_h \approx N_A -N_D$ and $n_e = n_i^2/(N_A-N_D)$.

We can now easily find the Fermi level:

$$E_F = E_G - kT\log[N_C/(N_D-N_A)], \textrm{ for } N_D > N_A$$
and
$$E_F = kT\log[N_V/(N_A-N_D)], \textrm{ for } N_A > N_D$$

**Question:** When is a semiconductor intrinsic, and when it is extrinsic?

**Answer:** The semiconductor is intrinsic when $|N_D-N_A| \ll n_i$, so $kT \gtrsim E_G/\log[N_C N_V/(N_D-N_A)^2]$.

## Summary

Density of states in a doped semiconductor:

![](figures/doping.svg)

Charge balance determins the number of electrons and holes as well as the position of the Fermi level.

If dopant concentrations are low, then $n_e = n_h = n_i \equiv \sqrt{N_C N_V}e^{-E_G/2kT}$.

If dopant concentrations are high, then in $n$-doped semiconductor $n_e = N_D - N_A$ and $n_h = n_i^2/n_e$ (or vice versa).
