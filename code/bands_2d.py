import numpy as np
import plotly.offline as py
import plotly.graph_objs as go


pi = np.pi

def E(k_x, k_y):
    delta = np.array([-2*pi, 0, 2*pi])
    H = np.diag(
        ((k_x + delta)[:, np.newaxis]**2
        + (k_y + delta)[np.newaxis]**2).flatten()
    )
    return tuple(np.linalg.eigvalsh(H + 5)[:3])

E = np.vectorize(E, otypes=(float, float, float))


figure_html = """
<head>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<body>{figure}
</body>
""".format


def plot_nfem():
    momenta = np.linspace(-2*pi, 2*pi, 100)
    kx, ky = momenta[:, np.newaxis], momenta[np.newaxis, :]
    bands = E(kx, ky)

    # Extended Brillouin zone scheme
    pad = .3
    first_BZ = ((abs(kx) < pi + pad) & (abs(ky) < pi + pad))
    second_BZ = (
        ((abs(kx) > pi - pad) | (abs(ky) > pi - pad))
        & ((abs(kx + ky) < 2*pi + pad) & (abs(kx - ky) < 2*pi + pad))
    )
    third_BZ = (
        (abs(kx + ky) > 2*pi - pad) | (abs(kx - ky) > 2*pi - pad)
    )

    bands[0][~first_BZ] = np.nan
    bands[1][~second_BZ] = np.nan
    #bands[2][~third_BZ] = np.nan

    # Actually plotting

    fig = go.Figure(
        data = [
            go.Surface(
                z=band / 5,
                colorscale=color,
                opacity=opacity,
                showscale=False,
                hoverinfo=False,
                x=momenta,
                y=momenta,
            )
            for band, color, opacity 
            in zip(bands[:2], 
                   ['#cf483d', '#3d88cf'], 
                   (1, 0.9))
        ],
        layout = go.Layout(
            title='Nearly free electrons in 2D',
            autosize=True,
            width=500,
            height=500,
            hovermode=False,
            scene=dict(
                yaxis={"title": "k_y"},
                xaxis={"title": "k_x"},
                zaxis={"title": "E"},
            )
        )
    )
    with open('docs/figures/nfem_2d.html', 'w') as f:
        f.write(figure_html(figure=
            py.plot(fig, show_link=False, 
                    output_type='div', include_plotlyjs=False)
        ))


def plot_tb():
    momenta = np.linspace(-pi, pi, 100)
    kx, ky = momenta[:, np.newaxis], momenta[np.newaxis, :]
    energies = -np.cos(kx) - np.cos(ky)
    fig = go.Figure(
        data = [
            go.Surface(
                z=energies,
                colorscale='#3d88cf',
                opacity=1,
                showscale=False,
                hoverinfo=False,
                x=momenta,
                y=momenta,
            )
        ],
        layout = go.Layout(
            title='Tight-binding in 2D',
            autosize=True,
            width=500,
            height=500,
            hovermode=False,
            scene=dict(
                yaxis={"title": "k_y"},
                xaxis={"title": "k_x"},
                zaxis={"title": "E"},
            )
        )
    )
    with open('docs/figures/tb_2d.html', 'w') as f:
        f.write(figure_html(figure=
            py.plot(fig, show_link=False, 
                    output_type='div', include_plotlyjs=False)
        ))
    

if __name__ == '__main__':
    plot_nfem()
    plot_tb()
