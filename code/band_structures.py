import os

import numpy as np
import matplotlib
import mpl_toolkits
matplotlib.use('agg')
from matplotlib import pyplot

matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['figure.figsize'] = (8, 5)
matplotlib.rcParams['font.size'] = 16
matplotlib.rcParams['savefig.transparent'] = True
pi = np.pi

def draw_classic_axes(ax, x=0, y=0, xlabeloffset=.1, ylabeloffset=.07):
    ax.set_axis_off()
    x0, x1 = ax.get_xlim()
    y0, y1 = ax.get_ylim()
    ax.annotate(ax.get_xlabel(), xytext=(x1, y), xy=(x0, y),
            arrowprops=dict(arrowstyle="<-"), va='center')
    ax.annotate(ax.get_ylabel(), xytext=(x, y1), xy=(x, y0),
            arrowprops=dict(arrowstyle="<-"), ha='center')
    for pos, label in zip(ax.get_xticks(), ax.get_xticklabels()):
        ax.text(pos, y - xlabeloffset, label.get_text(),
                ha='center', va='bottom')
    for pos, label in zip(ax.get_yticks(), ax.get_yticklabels()):
        ax.text(x - ylabeloffset, pos, label.get_text(),
                ha='right', va='center')

def fermi_dirac():
    fig = pyplot.figure()
    ax = fig.add_subplot(1,1,1)
    xvals = np.linspace(0, 2, 200)
    mu = .75
    beta = 20
    ax.plot(xvals, xvals < mu, ls='dashed', label='$T=0$')
    ax.plot(xvals, 1/(np.exp(beta * (xvals-mu)) + 1),
            ls='solid', label='$T>0$')
    ax.set_xlabel(r'$\varepsilon$')
    ax.set_ylabel(r'$f(\varepsilon, T)$')
    ax.set_yticks([0, 1])
    ax.set_yticklabels(['$0$', '$1$'])
    ax.set_xticks([mu])
    ax.set_xticklabels([r'$\mu$'])
    ax.set_ylim(-.1, 1.1)
    ax.legend()
    draw_classic_axes(ax)
    pyplot.tight_layout()
    fig.savefig('fermi_dirac.svg')
    
def phonons_1d():
    k = np.linspace(-2*pi, 6*pi, 500)
    fig, ax = pyplot.subplots()
    
    pyplot.plot(k, np.abs(np.sin(k/2)))
    
    ax.set_ylim(bottom=0, top=1.2)
    ax.set_xlabel('$ka$')
    ax.set_ylabel(r'$\omega$')
    pyplot.xticks(list(2*pi*np.arange(-1, 4)) + [-pi, pi],
                  [r'$-2\pi$', '$0$', r'$2\pi$', r'$4\pi$', r'$6\pi$',
                   r'$-\pi$', r'$\pi$'])
    pyplot.yticks([1], [r'$2\sqrt{\frac{\kappa}{m}}$'])
    pyplot.vlines([-pi, pi], 0, 1.1, linestyles='dashed')
    pyplot.hlines([1], .1*pi, 1.3*pi, linestyles='dashed')
    draw_classic_axes(ax)
    ax.annotate(s='', xy=(-pi, -.15), xytext=(pi, -.15),
                arrowprops=dict(arrowstyle='<->', shrinkA=0, shrinkB=0))
    ax.text(0, -.25, '1st Brillouin zone', ha='center')
    ax.set_ylim(bottom=-.7)
    pyplot.savefig('phonons3.svg')


def tight_binding_1d():
    pyplot.figure()
    k = np.linspace(-pi, pi, 300)
    pyplot.plot(k, -np.cos(k))
    pyplot.xlabel('$ka$'); pyplot.ylabel('$E$')
    pyplot.xticks([-pi, 0, pi], ['$-\pi$', 0, '$\pi$'])
    pyplot.yticks([-1, 0, 1], ['$E_0+2t$', '$E_0$', '$E_0-2t$'])
    pyplot.savefig('tight_binding_1d.svg')


def meff_1d_tb():
    pyplot.figure(figsize=(8, 5))
    k = np.linspace(-pi, pi, 300)
    meff = 1/np.cos(k)
    color = list(matplotlib.rcParams['axes.prop_cycle'])[0]['color']
    pyplot.plot(k[meff > 0], meff[meff > 0], c=color)
    pyplot.plot(k[meff < 0], meff[meff < 0], c=color)
    pyplot.ylim(-5, 5)
    pyplot.xlabel('$ka$'); pyplot.ylabel('$m_{eff}$')
    pyplot.xticks([-pi, 0, pi], ['$-\pi$', 0, '$\pi$']);
    pyplot.savefig('meff_1d_tb.svg')


def dispersion_2m(k, kappa=1, M=1.4, m=1, acoustic=True):
    Mm = M*m
    m_harm = (M + m) / Mm
    root = kappa * np.sqrt(m_harm**2 - 4*np.sin(k/2)**2 / Mm)
    if acoustic:
        root *= -1
    return np.sqrt(kappa*m_harm + root)
    

def phonons_1d_2masses():
        k = np.linspace(-2*pi, 6*pi, 300)
        fig, ax = pyplot.subplots()
        ax.plot(k, dispersion_2m(k, acoustic=False), label='optical')
        ax.plot(k, dispersion_2m(k), label='acoustic')
        ax.set_xlabel('$ka$')
        ax.set_ylabel(r'$\omega$')
        pyplot.xticks([-pi, 0, pi], ['$-\pi$', '$0$', '$\pi$'])
        pyplot.yticks([], [])
        pyplot.vlines([-pi, pi], 0, 2.2, linestyles='dashed')
        pyplot.legend()
        pyplot.xlim(-1.75*pi, 3.5*pi)
        pyplot.ylim(bottom=0)
        draw_classic_axes(ax, xlabeloffset=.2)
        pyplot.savefig('phonons6.svg')

def DOS_finite_phonon_chain(n, output_name):
    rhs = 2 * np.eye(n) - np.eye(n, k=1) - np.eye(n, k=-1)
    rhs[0, 0] -= 1
    rhs[-1, -1] -= 1
    pyplot.figure()
    pyplot.hist(np.sqrt(np.abs(np.linalg.eigvalsh(rhs))), bins=30)
    pyplot.xlabel("$\omega$")
    pyplot.ylabel("Number of levels")
    pyplot.savefig(output_name)


def DOS_finite_electron_chain(n, output_name):
    rhs = - np.eye(n, k=1) - np.eye(n, k=-1)
    pyplot.figure()
    pyplot.hist(np.linalg.eigvalsh(rhs), bins=30)
    pyplot.xlabel("$E$")
    pyplot.ylabel("Number of levels")
    pyplot.savefig(output_name)


def main():
    os.chdir('docs/figures')
    fermi_dirac()
    phonons_1d()
    phonons_1d_2masses()
    meff_1d_tb()
    tight_binding_1d()
    DOS_finite_phonon_chain(3, '3_phonons.svg')
    DOS_finite_phonon_chain(300, '300_phonons.svg')
    DOS_finite_electron_chain(3, '3_electrons.svg')
    DOS_finite_electron_chain(300, '300_electrons.svg')

if __name__ == "__main__":
    main()
